#
# Commands used when setting up box
#

zypper install -y nano

# Select 2 followed by y, ja and ja
zypper install -t pattern kde4 kde4_basis


# Edit the file /etc/sysconfig/displaymanager and set DISPLAYMANAGER="kdm"
nano /etc/sysconfig/displaymanager

# This don't work, need to check how this is done from a script (I'vd done it before"
sudo su root -c "echo XXX | passwd"
